class DetailReportCardsController < ApplicationController
  before_action :find_detail_report_card, only: [:update]
  def update
    if @detail_report_card.update detail_report_card_params
      
      render json: nil, status: :ok      
    end
  end
  private 
  def detail_report_card_params
    params.permit(:report_card_id,:course_id,:calification)
  end
  def find_detail_report_card
    @detail_report_card = DetailReportCard.find(params[:id])
  end
end
