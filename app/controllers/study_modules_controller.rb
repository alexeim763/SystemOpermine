class StudyModulesController < ApplicationController
  before_action :find_study_module, only: [:show, :edit, :update, :destroy]
  def index
    @study_modules = StudyModule.all
  end
  def new
    @study_module = StudyModule.new
    @courses = Course.all
  end
  def create
    @study_module = StudyModule.new study_module_params
    if @study_module.save
      redirect_to study_modules_path, notice: 'Se creó correctamente'
    else
      render 'new', notice: 'Ocurrio un error'
    end
  end
  def edit
    @courses = Course.all
  end
  def show

  end
  def update
    if @study_module.update study_module_params
      redirect_to study_modules_path, notice: 'se actualizo correctamente'
    else
      redirect_to 'edit', notice: 'Ocurrio algun error'
    end
  end
  def destroy
     @study_module.destroy
     redirect_to study_modules_path   
  end
  private
  def study_module_params
    params.require(:study_module).permit(:name, {course_ids:[]})
  end
  def find_study_module
    @study_module = StudyModule.find(params[:id])
  end
end
