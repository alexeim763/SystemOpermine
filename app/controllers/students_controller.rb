class StudentsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_student, only: [:show, :edit, :destroy, :update]
  def index
    @students = Student.all
    if params[:search]
      @students = Student.search(params[:search]).order("created_at DESC")
    else
      @students = Student.all
    end
  end 
  def new 
    @student = Student.new
  end
  def show

  end
  def create
    @student = Student.new student_params
    if @student.save
      
      generateReportCard(@student)
      redirect_to students_path, notice: "Bien, se creo el estudiante"
    else 
      render 'new', notice: "Ocurrió un error, por favor verique bien los datos"
    end
  end
  def edit
  end
  def update
    if @student.update student_params
      redirect_to @student, notice: 'Se actualizo correctamente'
    else
      redirect_to 'edit', notice: 'Ocurio algun tipo de error'
    end
  end
  def destroy
    @student.destroy
    redirect_to students_path
  end


  private
  def student_params
    params.require(:student).permit(:code,:firstName,:lastName,:age, :dni, :firstModule)
  end

  def find_student
    @student = Student.find(params[:id])
  end

  def generateReportCard(student)
    
    @study_modules = StudyModule.all
    @study_modules.each do |study_module|
      @report_card = ReportCard.new
      @report_card.student = student
      @report_card.study_module = study_module
      @report_card.save
      study_module.courses.each do |course|
        @detail_report_card = DetailReportCard.new
        @detail_report_card.report_card = @report_card
        @detail_report_card.course = course
        @detail_report_card.calification = 0  
        @detail_report_card.save
      end
    end

  end
end
