class ReportCardsController < ApplicationController
  before_action :find_student
  before_action :find_report_card, only: [:show, :edit, :update, :destroy]
  def index
    @report_cards = @student.report_cards
    @num = 0
  end
  def show
    @detail_report_cards = @report_card.detail_report_cards
    @num = 0
    @final_average = 0
    totalNumber = 0
    number_courses = @detail_report_cards.length
    @detail_report_cards.each do |detail|
      totalNumber = totalNumber + detail.calification
    end
    @final_average = totalNumber/number_courses
    @report_card.final_average = @final_average
    @report_card.save


  end
  def edit
    @detail_report_cards = @report_card.detail_report_cards
  end

  private
  def find_student
    @student = Student.find(params[:student_id])
    
  end
  def find_report_card
    @report_card = ReportCard.find(params[:id])
  end
end
