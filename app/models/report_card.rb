class ReportCard < ActiveRecord::Base
  belongs_to :student
  belongs_to :study_module
  has_many :detail_report_cards
end
