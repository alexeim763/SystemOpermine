class StudyModule < ActiveRecord::Base
  has_many :report_cards
  has_and_belongs_to_many :courses
end
