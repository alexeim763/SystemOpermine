class Course < ActiveRecord::Base
  has_many :detail_report_cards, dependent: :destroy
  has_and_belongs_to_many :study_modules, dependent: :destroy
end
