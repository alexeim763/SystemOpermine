class Student < ActiveRecord::Base
  has_many :report_cards
  def self.search(search)
    where("firstName LIKE ? OR lastName LIKE ? OR dni LIKE ? ", "%#{search}","%#{search}","%#{search}")
  end
end
