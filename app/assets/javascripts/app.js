$(document).on('ready page:load', function () {
  var $calification = $('.calification');
  $calification.on('click', function() {
    $(this).select();
  });
  $calification.on('blur', function (){
    var $element = $(this)
    var id_report_card = $element.data('detail');
    var id_course = $element.data('course');
    var value_calification = $element.val();
    console.log(id_report_card, value_calification);
    if(value_calification > 20 || value_calification < 0){
      alert('No puede ingresar esos valores');
      $element.select();
      $element.focus();
    }
    else
    {
      $.ajax({
        url: 'detalle-boleta/' + id_report_card,
        method: 'PUT',
        dataType: "script",
        data: {report_card_id: id_report_card, course_id: id_course, calification: value_calification},
        success: function(data) {
          console.log("Actualizado");
          console.log(data);
        }
      })
    }

  });

});