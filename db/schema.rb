# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151111132015) do

  create_table "courses", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "courses_study_modules", force: :cascade do |t|
    t.integer "study_module_id", limit: 4
    t.integer "course_id",       limit: 4
  end

  add_index "courses_study_modules", ["course_id"], name: "index_courses_study_modules_on_course_id", using: :btree
  add_index "courses_study_modules", ["study_module_id"], name: "index_courses_study_modules_on_study_module_id", using: :btree

  create_table "detail_report_cards", force: :cascade do |t|
    t.integer  "report_card_id", limit: 4
    t.integer  "course_id",      limit: 4
    t.integer  "calification",   limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "detail_report_cards", ["course_id"], name: "index_detail_report_cards_on_course_id", using: :btree
  add_index "detail_report_cards", ["report_card_id"], name: "index_detail_report_cards_on_report_card_id", using: :btree

  create_table "report_cards", force: :cascade do |t|
    t.integer  "student_id",      limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "study_module_id", limit: 4
    t.integer  "final_average",   limit: 4
  end

  add_index "report_cards", ["student_id"], name: "index_report_cards_on_student_id", using: :btree
  add_index "report_cards", ["study_module_id"], name: "index_report_cards_on_study_module_id", using: :btree

  create_table "students", force: :cascade do |t|
    t.integer  "code",        limit: 4
    t.string   "firstName",   limit: 255
    t.string   "lastName",    limit: 255
    t.integer  "age",         limit: 4
    t.string   "dni",         limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "firstModule", limit: 255
  end

  create_table "study_modules", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "courses_study_modules", "courses"
  add_foreign_key "courses_study_modules", "study_modules"
  add_foreign_key "detail_report_cards", "courses"
  add_foreign_key "detail_report_cards", "report_cards"
  add_foreign_key "report_cards", "students"
  add_foreign_key "report_cards", "study_modules"
end
