class AddToReportCard < ActiveRecord::Migration
  def change
    add_column :report_cards, :final_average, :integer
  end
end
