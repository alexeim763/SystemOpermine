class CreateCoursesStudyModules < ActiveRecord::Migration
  def change
    create_table :courses_study_modules do |t|
      t.references :study_module, index: true, foreign_key: true
      t.references :course, index: true, foreign_key: true
    end
  end
end
