class CreateDetailReportCards < ActiveRecord::Migration
  def change
    create_table :detail_report_cards do |t|
      t.references :report_card, index: true, foreign_key: true
      t.references :course, index: true, foreign_key: true
      t.integer :calificacion

      t.timestamps null: false
    end
  end
end
