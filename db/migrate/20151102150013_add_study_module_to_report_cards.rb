class AddStudyModuleToReportCards < ActiveRecord::Migration
  def change
    add_reference :report_cards, :study_module, index: true, foreign_key: true
  end
end
