class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.integer :code
      t.string :firstName
      t.string :lastName
      t.integer :age
      t.string :dni

      t.timestamps null: false
    end
  end
end
