class CreateReportCards < ActiveRecord::Migration
  def change
    create_table :report_cards do |t|
      t.references :student, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
