class RenameColumnFromDetailReportCard < ActiveRecord::Migration
  def change
    rename_column :detail_report_cards, :calificacion , :calification
  end
end
