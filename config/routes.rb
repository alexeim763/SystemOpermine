Rails.application.routes.draw do
  get 'report_cards/index'

  devise_for :users
  root 'pages#index'
  resources :students, path: 'alumnos' do     
    resources :report_cards, path: 'boletas' do
      resources :detail_report_cards, path: 'detalle-boleta'
    end
  end
  resources :courses, path: 'cursos'
  resources :study_modules, path: 'modulos'


end
